// Includes relevant modules used by the QML
import QtQuick 2.15
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.20 as Kirigami

import org.kde.spork 1.0

// Provides basic features needed for all kirigami applications
Kirigami.ApplicationWindow {
    // Unique identifier to reference this object
    id: root

    // Window title
    // i18nc() makes a string translatable
    // and provides additional context for the translators
    title: i18nc("@title:window", "Spork")

    // Set the first page that will be loaded when the app opens
    // This can also be set to an id of a Kirigami.Page
    //    pageStack.initialPage: Kirigami.Page {
         // Add widgets here..
    globalDrawer: Kirigami.GlobalDrawer {

        header: Kirigami.AbstractApplicationHeader {

            contentItem: Controls.ComboBox {
                id: availablePorts
                width: 200
                anchors.leftMargin: 5
                model: Spork.portNames
                Layout.fillWidth: true
            }

            Controls.Button {
                id: update
                text: "Update"
                anchors.leftMargin: 5
                anchors.left: availablePorts.right
                onClicked: Spork.updatePortList()
            }

        }

        actions: [
                Kirigami.Action {
                    text: "Port Details"
                },
                //Kirigami.Action {
                //    text: "Kirigami Action 2"
                //},
                Kirigami.Action {
                    text: i18n("Quit")
                    icon.name: "application-exit"
                    shortcut: StandardKey.Quit
                    onTriggered: Qt.quit()
                }
        ]
    }

}
//}
