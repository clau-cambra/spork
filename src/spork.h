#pragma once

#include <QDebug>

#include <QObject>
#include <QList>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QVariant>

class Spork : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariantList portNames READ portNames NOTIFY portsUpdated)

public:
    explicit Spork (QObject *parent = nullptr);
    QList<QSerialPortInfo> getPortList();
    QVariantList portNames() const;

signals:
    void portsUpdated();

public slots:
    void updatePortList();

private:
    QList<QSerialPortInfo> m_availablePorts;
};
