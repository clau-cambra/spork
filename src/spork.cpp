#include  "spork.h"

Spork::Spork(QObject *parent) : QObject (parent)
{
    m_availablePorts = QSerialPortInfo::availablePorts();
}

void Spork::updatePortList()
{
    int i;
    bool same=true;
    QList<QSerialPortInfo> newPorts = QSerialPortInfo::availablePorts();
    if (m_availablePorts.size() == newPorts.size())
    {
        for (i=0;i<newPorts.size();i++)
        {
            if (
                std::forward_as_tuple(  newPorts[i].description(),
                                        newPorts[i].manufacturer(),
                                        newPorts[i].portName(),
                                        newPorts[i].productIdentifier(),
                                        newPorts[i].serialNumber())
                !=
                std::forward_as_tuple(  m_availablePorts[i].description(),
                                        m_availablePorts[i].manufacturer(),
                                        m_availablePorts[i].portName(),
                                        m_availablePorts[i].productIdentifier(),
                                        m_availablePorts[i].serialNumber())
            )
            {same=false;}
        }
        if (same) {return;}
    }

    m_availablePorts = newPorts;
    Q_EMIT portsUpdated();

}

QList<QSerialPortInfo> Spork::getPortList()
{
    return m_availablePorts;
}

QVariantList Spork::portNames() const
{
    QVariantList portNames;
    foreach(QSerialPortInfo port, m_availablePorts){
        portNames << port.portName();
    }

    return portNames;
}
